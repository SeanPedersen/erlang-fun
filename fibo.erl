% Compute N-th element of the Fibonacci sequence.
% Try out fibo:fib(42). vs fibo:fib_(42) to see the runtime difference!

-module(fibo).
-author("Sean Pedersen").
-compile(export_all).

% Head recursion (inefficient: builds up exponentially growing function call stack: 2^N)
fib(1) -> 1;
fib(2) -> 1;
fib(N) when N > 1 -> fib(N-1) + fib(N-2).

% Tail recursion (efficient: behaves like a loop)
% The tail recursive optimization lets the function call stack not grow at all)
fib_(N) -> fib(N, 0, 1).
fib(1, _First, Second) -> Second;
fib(N, First, Second) when N > 1 -> fib(N-1, Second, First+Second).

% Compute N-th element of Fibonacci sequence of order X
fib_n(N, OrderX) -> fib_n_(N, range(OrderX)).
fib_n_(1, OrderXList) -> [H|_T] = lists:reverse(OrderXList), H;
fib_n_(N, OrderXList) when N > 1 ->
    Next = sum(OrderXList),
    [_H|NewOXL] = OrderXList,
    fib_n_(N-1, NewOXL++[Next]).

sum(L) -> sum(L, 0).
sum([], Sum) -> Sum;
sum([H|T], Sum) -> sum(T, Sum+H).

range(N) -> range(N, []).
range(0, RangeL) -> RangeL;
range(N, RangeL) -> range(N-1, [N-1|RangeL]).
