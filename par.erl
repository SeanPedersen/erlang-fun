-module(par).
-author("Sean Pedersen").
-compile(export_all).
% Parallel stuff

% Sequential map function (preserves list order)
% http://erldocs.com/18.0/stdlib/lists.html#map/2
map(_, []) -> [];
map(F, [H|T]) -> [F(H)|map(F, T)].

% Multi-process map function (preserves list order)
pmap(F, L) ->
    S = self(),
    Ref = erlang:make_ref(),
    Pids = map(fun(I) ->
                    spawn(fun() -> do_f(S, Ref, F, I) end)
                end, L),
    gather(Pids, Ref).

do_f(Parent, Ref, F, I) ->
    Parent ! {self(), Ref, (catch F(I))}.

gather([Pid|T], Ref) ->
    receive
        {Pid, Ref, Ret} -> [Ret|gather(T, Ref)]
    end;
gather([], _) -> [].

% Sequential filter function (preserves list order)
% http://erldocs.com/18.0/stdlib/lists.html#filter/2
filter(Fun, List) -> lists:reverse(filter(Fun, List, [])).
filter(Fun, [H|T], Accu) ->
    case Fun(H) of
        true -> filter(Fun, T, [H|Accu]);
        false -> filter(Fun, T, Accu)
    end;
filter(_Fun, [], Accu) -> Accu.
