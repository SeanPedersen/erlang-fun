% Linked processes commiting group suicide
% Links are bidirectional! Linked processes die together.
-module(linkmon).
-compile(export_all).


chain(0) ->
    receive
        _ -> ok
        after 2000 ->
        exit("chain dies here")
    end;

chain(N) ->
    Pid = spawn_link(fun() -> chain(N-1) end),
    receive
        _ -> Pid, ok
    end.