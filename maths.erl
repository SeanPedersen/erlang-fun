% QUICK MATHEMATICS with ERLANG
-module(maths).
-compile(export_all).


fac(1) -> 1;
fac(N) -> N * fac(N-1).

ggT(X, 0) -> X;
ggT(X, Y) -> ggT(Y, X rem Y).

prime(1) -> false;
prime(2) -> true;
prime(N) when N rem 2 == 0 -> false;
prime(N) -> prime(N, 3).
prime(N, M) ->
    case M > math:sqrt(N) of
        false -> case N rem M == 0 of
                    true -> false;
                    false -> prime(N, M+1)
                 end;
        true -> true
    end.
