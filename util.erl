% Utility functions for Erlang
-module(util).
-author("Sean Pedersen").
-export([i/2, head/1, tail/1, range/1, range/2, len/1, random_float/0, random/1, str/1, write/2, write/3, printL/1, print/1, type/1, log/2, logstop/0, map/2, pmap/2]).


% Index element access to list
i(L, Index) when Index < 0 -> i_(lists:reverse(L), Index);
i(L, Index) -> i_(L, Index).

i_([H|_T], 0) -> H;
i_([H|_T], -1) -> H;
i_([_H|T], Index) when Index > 0 -> i_(T, Index-1);
i_([_H|T], Index) when Index < 0 -> i_(T, Index+1).


% Return head of list
head([H|_T]) -> H.


% Return tail of list
tail([_H|T]) -> T.


% List from 0 to N-1 (returned list length is N)
range(N) -> lists:seq(0, N-1).


% List from N to M
range(N, M) -> lists:seq(N, M).


% Return length of list
len(L) -> len(L, 0).
len([_H|T], Size) -> len(T, Size+1);
len([], Size) -> Size.

% Return random float within 0.0-1.0
random_float() -> rand:uniform().


% Returns random (uniform distribution) element of list
random(L) -> i(L, rand:uniform(len(L))-1).


% Returns NoString as string (there are technically no strings in erlang, just lists)
str(NoString) ->
	% Evals true for lists which are "strings" (ASCII lists like f.e. [65,66,67])
	case io_lib:printable_list(NoString) of
		false -> String = lists:flatten(io_lib:format("~p", [NoString]));
		true -> String = NoString
	end,
	String.


% Write content to file
write(Filename, Content) -> write(Filename, Content, append).
write(Filename, Content, Mode) -> file:write_file(Filename, Content, [Mode]).


% Prints list of Things as one string seperated with spaces
printL(Things=[_H|_T]) ->
	StrList = str_spaces(Things),
	String = lists:concat(StrList),
	io:format(String++"~n").

str_spaces([H]) -> [str(H)];
str_spaces([H|T]) -> [str(H)++" "|str_spaces(T)].


% Just a simple print of Thing followed by a new line
print(Thing) ->
	io:format(str(Thing)++"~n").


% Returns type of Something
type(Something) ->
    if is_atom(Something) -> atom;
	   is_binary(Something) -> binary;
	   is_bitstring(Something) -> bitstring;
	   is_boolean(Something) -> boolean;
	   is_float(Something) -> float;
	   is_function(Something) -> function;
	   is_integer(Something) -> integer;
	   is_list(Something) -> list;
	   is_number(Something) -> number;
	   is_pid(Something) -> pid;
	   is_port(Something) -> port;
	   is_reference(Something) -> reference;
	   is_tuple(Something) -> tuple
	end.


% Logs Content into File (concurrently)
log(File, Content) -> Known = erlang:whereis(log_proc),
						 case Known of
							undefined -> PIDlog_proc = spawn(fun() -> logloop(0) end),
										 erlang:register(log_proc, PIDlog_proc);
							_NotUndef -> ok
						 end,
						 log_proc ! {File, Content},
						 ok.
% Kills concurennt logging process
logstop() -> 	Known = erlang:whereis(log_proc),
				case Known of
					undefined -> false;
					_NotUndef -> log_proc ! kill, 
								erlang:unregister(log_proc),
								true
				end.
% Logging receive function				
logloop(Y) -> 	receive
					{File,Content} -> io:format(Content),
									  file:write_file(File, Content, [append]),
									  logloop(Y+1);
					kill -> true
				end.


% Sequential map function (preserves list order)
map(_, []) -> [];
map(F, [H|T]) -> [F(H)|map(F, T)].


% Multi-process map function (preserves list order)
pmap(F, L) ->
    S = self(),
    Ref = erlang:make_ref(),
    Pids = map(fun(I) ->
                    spawn(fun() -> do_f(S, Ref, F, I) end)
                end, L),
    gather(Pids, Ref).

do_f(Parent, Ref, F, I) ->
    Parent ! {self(), Ref, (catch F(I))}.

gather([Pid|T], Ref) ->
    receive
        {Pid, Ref, Ret} -> [Ret|gather(T, Ref)]
    end;
gather([], _) -> [].
