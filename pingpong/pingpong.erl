% Source: http://erlang.org/doc/getting_started/conc_prog.html
% Ping pong across two machines/nodes in the same local network
% pingpong.erl must be compiled in working dir of the erlang shells on both machines!
% ---STARTUP INSTRUCTIONS---
% node1:
% $ erl -compile pingpong
% $ erl -sname pong -setcookie zummsel
% node2:
% $ erl -compile pingpong
% $ erl -sname ping -setcookie zummsel
% pong@node2> pingpong:start(ping@welle).

-module(pingpong).

-export([start/1,  ping/3, pong/0]).

ping(0, PongName, PongNode) ->
    {PongName, PongNode} ! finished,
    io:format("Ping finished~n", []);

ping(N, PongName, PongNode) ->
    {PongName, PongNode} ! {ping, self()},
    receive
        pong ->
            io:format("Ping received pong~n", [])
    end,
    ping(N - 1, PongName, PongNode).

pong() ->
    receive
        finished ->
            io:format("Pong finished~n", []);
        {ping, PingPID} -> % Note that PingPID is enough to reply (PID contains info about its node)
            io:format("Pong received ping~n", []),
            PingPID ! pong,
            pong()
    end.

start(PingNode) ->
    PongName = pong,
    register(PongName, spawn(pingpong, pong, [])),
    % Spawn function ping/3 remotely on PingNode machine
    spawn(PingNode, pingpong, ping, [3, PongName, node()]).
